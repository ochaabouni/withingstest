//
//  PreviewViewModel.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 12/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class PreviewViewModel {
    
    var delegate: PreviewCoordinatorDelegate?
    private var previewItems: [ImageItem]
    
    init(previews: [ImageItem]) {
        self.previewItems = previews
    }
    
    func getPreviewList() -> [String] {
        return self.previewItems.compactMap({ $0.largeImageURL })
    }
    
    func closePreview() {
        delegate?.exitPreview()
    }
}
