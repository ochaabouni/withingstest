//
//  ListingsViewModel.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class GalleryViewModel {
    
    var delegate: GalleryCoordinatorDelegate?
    let service: ServerProvider
    var searchResult: (([ImageItem]) -> Void)?
    private var images: [ImageItem] = []
    private var numberOfListingPerTime = 20
    private var timer: Timer?
    private var searchedText: String?
    
    init(_ service: ServerProvider) {
        self.service = service
    }
    
    @objc private func searchPhotos() {
        guard let keyword = self.searchedText else {
            searchResult?([])
            return
        }
        guard !keyword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            searchResult?([])
            return
        }
        service.searchPhotos(keyword) { (images, error) in
            guard error == nil else {
                self.searchResult?([])
                return
            }
            self.images = images ?? []
            if self.images.count < self.numberOfListingPerTime {
                self.searchResult?(self.images)
            } else {
                self.searchResult?(Array(self.images[0..<self.numberOfListingPerTime]))
            }
        }
    }
    
    func didEditSearch(_ text: String) {
        self.searchedText = text
        if let timer = self.timer {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchPhotos), userInfo: nil, repeats: false)
    }
    
    
    func fetchNextData(_ startIndex: Int, _ completion: @escaping (([ImageItem]) -> Void)) {
        if startIndex + numberOfListingPerTime < images.count {
            completion(Array(images[startIndex ..< startIndex + numberOfListingPerTime]))
        } else {
            completion(Array(images[startIndex ..< images.count]))
        }
    }
    
    func showPreview(_ selectedIndexes: [Int]) {
        let selectedItems: [ImageItem] = selectedIndexes.reduce(into: []) { (result, index) in
            result.append(self.images[index])
        }
        delegate?.showPreview(selectedItems)
    }
    
}
