//
//  GalleryImageableViewCell.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class GalleryImageCollectionViewCell: UICollectionViewCell {

    static let identifier = "GalleryImageCollectionViewCell"

    override var reuseIdentifier: String? {
        return GalleryImageCollectionViewCell.identifier
    }
    
    var imageItem: ImageItem? {
        didSet {
            self.configureListing(imageItem)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.selectedImage.isHidden = !isSelected
        }
    }

    lazy private var hitsLabel: UIPaddedLabel = {
        let label = UIPaddedLabel(topInset: 2, bottomInset: 2, leftInset: 8, rightInset: 8)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ThemeManager.urgentTintColor
        label.layer.borderWidth = 1
        label.layer.borderColor = ThemeManager.urgentTintColor?.cgColor
        label.layer.cornerRadius = 3
        return label
    }()

    lazy private var itemImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    lazy private var imageContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.addSubview(itemImage)
        itemImage.fit(view)

        view.addSubview(hitsLabel)
        hitsLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
        hitsLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        view.addSubview(selectedImage)
        selectedImage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8).isActive = true
        selectedImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        selectedImage.isHidden = true
        return view
    }()
    
    lazy private var selectedImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "selected")
        imageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return imageView
    }()

    private func initViews() {

        self.backgroundColor = .clear

        // container
        addSubview(imageContainer)
        imageContainer.fit(self)

    }

    private func configureListing(_ item: ImageItem?) {
        guard let item = item else {
            return
        }
        if let hits = item.likes {
            self.hitsLabel.text = "♥ \(hits)"
        } else {
            self.hitsLabel.isHidden = true
        }
        self.itemImage.loadImage(item.largeImageURL ?? "", placeHolder: UIImage(named: "placeholder"))

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        self.itemImage.image = UIImage(named: "placeholder")
        self.configureListing(self.imageItem)
        self.selectedImage.isHidden = !isSelected
    }
}
