//
//  PreviewViewController.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 12/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
    
    private var viewModel: PreviewViewModel!
    private var images: [String] = []
    private var timer: Timer?
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        loadImages()
    }
    
    func setViewModel(_ viewModel: PreviewViewModel) {
        self.viewModel = viewModel
    }
    
    private func initViews() {
        self.title = TranslationManager.previewTitle
        setNavigationBarButton("black_cross", action: #selector(dismissView))
        view.addSubview(imageView)
        imageView.fit(view)
        view.backgroundColor = .black
    }
    
    @objc private func dismissView() {
        timer?.invalidate()
        viewModel.closePreview()
    }
    
    private func loadImages() {
        self.images = viewModel.getPreviewList()
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(startPreview), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    @objc private func startPreview() {
        if let random = images.randomElement() {
            self.imageView.loadImage(random, placeHolder: nil, animated: true)
        }
    }

}
