//
//  GalleryViewController.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {
    
    private var viewModel: GalleryViewModel!
    private var listings = [ImageItem]()
    private var isLoadingList = false
    
    lazy private var searchTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .roundedRect
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = TranslationManager.searchPlaceHolder
        textField.returnKeyType = .done
        textField.delegate = self
        textField.addTarget(self, action: #selector(searchTextDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy private var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
        collectionView.register(GalleryImageCollectionViewCell.self, forCellWithReuseIdentifier: GalleryImageCollectionViewCell.identifier)
        collectionView.backgroundColor = ThemeManager.mainBackgroundColor
        return collectionView
    }()
    
    lazy private var errorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        let imageView = UIImageView(image: UIImage(named: "ghost"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        label.text = TranslationManager.noData
        label.numberOfLines = 0
        label.textAlignment = .center
        
        let retryButton = UIButton()
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        retryButton.setTitle(TranslationManager.retry, for: .normal)
        retryButton.setTitleColor(ThemeManager.buttonTint, for: .normal)
        retryButton.addTarget(self, action: #selector(self.searchTextDidChange(_:)), for: .touchUpInside)
        view.addSubview(retryButton)
        retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 12).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return view
    }()
    
    lazy private var placeHolder: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        let imageView = UIImageView(image: UIImage(named: "keyboard"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        label.text = TranslationManager.gallleryPlaceHolder
        label.numberOfLines = 0
        label.textAlignment = .center
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
        self.onPhotosSearched()
    }
    
    func setViewModel(_ viewModel : GalleryViewModel) {
        self.viewModel = viewModel
    }
    
    private func onPhotosSearched() {
        
        viewModel.searchResult = { (images) in
            DispatchQueue.main.async {
                self.placeHolder.isHidden = true
                guard !images.isEmpty else {
                    self.errorView.isHidden = false
                    return
                }
                self.errorView.isHidden = true
                self.listings = images
                self.collectionView.reloadData()
                self.collectionView.indexPathsForSelectedItems?.forEach({ self.collectionView.deselectItem(at:$0, animated: false) })
            }
        }
    }
    
    private func initViews() {
        view.backgroundColor = ThemeManager.mainBackgroundColor
        self.title = TranslationManager.galleryTitle
        
        self.view.addSubview(searchTextField)
        searchTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 24).isActive = true
        searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 24).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        self.view.addSubview(errorView)
        errorView.topAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
        errorView.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
        errorView.leadingAnchor.constraint(equalTo: collectionView.leadingAnchor).isActive = true
        errorView.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor).isActive = true
        
        self.view.addSubview(placeHolder)
        placeHolder.topAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
        placeHolder.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
        placeHolder.leadingAnchor.constraint(equalTo: collectionView.leadingAnchor).isActive = true
        placeHolder.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor).isActive = true
        
    }
    
    private func shouldShowNavigationbutton(_ show: Bool) {
        if show {
            setNavigationBarButton("check", action: #selector(showPreview), isLeftBarButtonItem: false)
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc private func searchTextDidChange(_ sender: Any) {
        viewModel.didEditSearch(self.searchTextField.text ?? "")
    }
    
    @objc private func showPreview() {
        viewModel.showPreview(collectionView.indexPathsForSelectedItems?.compactMap({ $0.item }) ?? [])
    }

}

extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryImageCollectionViewCell.identifier, for: indexPath) as? GalleryImageCollectionViewCell {
            cell.imageItem = listings[indexPath.item]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? GalleryImageCollectionViewCell {
            cell.isSelected = !cell.isSelected
            self.listings[indexPath.item].isSelected = cell.isSelected
        }
        shouldShowNavigationbutton((collectionView.indexPathsForSelectedItems?.count ?? 0) >= 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        shouldShowNavigationbutton((collectionView.indexPathsForSelectedItems?.count ?? 0) >= 2)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList) {
                self.isLoadingList = true
                viewModel.fetchNextData(self.listings.count) { (list) in
                    guard !list.isEmpty else {
                        self.isLoadingList = false
                        return
                    }
                    self.listings.append(contentsOf: list)
                    self.collectionView.reloadData()
                    self.isLoadingList = false
                }
            }
        }
}


extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = UIScreen.main.bounds.width / 3
        return CGSize(width: itemSize, height: itemSize)
    }
}

extension GalleryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
