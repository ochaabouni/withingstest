//
//  ListingsCoordinator.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

protocol GalleryCoordinatorDelegate: class {
    func showPreview(_ items: [ImageItem])
}

protocol PreviewCoordinatorDelegate: class {
    func exitPreview()
}

class GalleryCoordinator: DeallocatableCoordinator {
    
    var terminate: (Deallocallable) -> Void = { _ in }
    
    var navigationController: UINavigationController
    
    var coordinators: [DeallocatableCoordinator] = []
    
    var window: UIWindow
    
    required init(_ window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func start() {
        coordinators.removeAll()
        let viewController = GalleryViewController()
        let viewModel = GalleryViewModel(ServerManager.shared)
        viewModel.delegate = self
        
        viewController.setViewModel(viewModel)
        navigationController.viewControllers = [viewController]
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func cancel() {
        terminate(self)
    }
    
    func showPreviewScreen(_ items: [ImageItem]) {
        let viewController = PreviewViewController()
        let viewModel = PreviewViewModel(previews: items)
        viewModel.delegate = self
        
        viewController.setViewModel(viewModel)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overFullScreen
        self.navigationController.present(navigationController, animated: true, completion: nil)
    }
    
    func closePresentedScreen() {
        self.navigationController.dismiss(animated: true, completion: nil)
    }
    
}

extension GalleryCoordinator: GalleryCoordinatorDelegate {
    
    func showPreview(_ items: [ImageItem]) {
        showPreviewScreen(items)
    }

}

extension GalleryCoordinator: PreviewCoordinatorDelegate {
    func exitPreview() {
        closePresentedScreen()
    }
}
