//
//  Coordinator.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit

typealias DeallocatableCoordinator = Coordinator & Deallocallable

protocol Coordinator: class {
    var navigationController: UINavigationController { get set }
    var coordinators: [DeallocatableCoordinator] { get set }
    var window: UIWindow { get }
    
    init(_ window: UIWindow, navigationController: UINavigationController)
    func start()
    func cancel()
    func startCoordinator<T>(_ coordinator: T.Type) where T: DeallocatableCoordinator
}

extension Coordinator {
    func startCoordinator<T>(_ coordinator: T.Type) where T: DeallocatableCoordinator {
        var childCoordinator = coordinator.init(window, navigationController: navigationController)
        childCoordinator.start()
        coordinators.append(childCoordinator)
        childCoordinator.terminate = { element in
            self.coordinators.removeAll(where: { $0.reference == element.reference })
        }
    }
}

protocol Deallocallable {
    
    var terminate: (Deallocallable) -> Void { get set}
    var reference: Int { get }
}
extension Deallocallable {
    
    var reference: Int {
        return unsafeBitCast(self, to: Int.self)
    }
    
}

