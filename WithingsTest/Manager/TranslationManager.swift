//
//  TranslationManager.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

struct TranslationManager {
    
    // dynamic localizing
    static func localize(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    
    static let retry = localize("retry")
    static let noData = localize("no_data")
    
    static let galleryTitle = localize("gallery_title")
    static let gallleryPlaceHolder = localize("keyboard_placeholder")
    static let searchPlaceHolder = localize("search_placeholder")
    static let previewTitle = localize("preview_title")
}
