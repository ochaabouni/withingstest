//
//  ImageView+AsyncLoading.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 03/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import UIKit
import QuartzCore

fileprivate let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
        
    func loadImage(_ URLString: String, placeHolder: UIImage?, animated: Bool = false) {
        
        self.image = placeHolder
        let imageServerUrl = URLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let cachedImage = imageCache.object(forKey: NSString(string: imageServerUrl)) {
            self.setImage(cachedImage, animated: animated)
            return
        }
        
        if let url = URL(string: imageServerUrl) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil {
                    debugPrint(error?.localizedDescription ?? "")
                    DispatchQueue.main.async {
                        self.image = placeHolder
                    }
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: imageServerUrl))
                            self.setImage(downloadedImage, animated: animated)
                        }
                    }
                }
            }).resume()
        }
    }
    
    func setImage(_ image: UIImage?, animated: Bool = true) {
        guard animated == true else {
            self.image = image
            return
        }
        let animation: UIView.AnimationOptions = [.transitionCrossDissolve, .transitionCurlDown, .transitionFlipFromTop, .transitionFlipFromBottom, .curveEaseInOut, .curveEaseIn, .curveEaseOut].randomElement() ?? .transitionCrossDissolve
       
        UIView.transition(with: self, duration: 1.0, options: animation, animations: {
            self.image = image
        }, completion: nil)
    }
}
