//
//  ImageItem.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 01/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

class ImagesResponse: Decodable {
    var hits: [ImageItem]?
}

class ImageItem: Decodable {
    var id: Int?
    var previewURL: String?
    var largeImageURL: String?
    var likes: Int?
    
    var isSelected: Bool = false
    
    
    private enum CodingKeys: String, CodingKey {
        case id, previewURL, largeImageURL, likes
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        likes = try? container.decode(Int.self, forKey: .likes)
        previewURL = try? container.decode(String.self, forKey: .previewURL)
        largeImageURL = try? container.decode(String.self, forKey: .largeImageURL)
    }
}
