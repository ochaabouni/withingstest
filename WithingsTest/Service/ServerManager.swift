//
//  ServerManager.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation


class ServerManager: ServerProvider {
        
    static let shared: ServerProvider = ServerManager()
    
    private let baseUrl = "https://pixabay.com/api/?key=18021445-326cf5bcd3658777a9d22df6f"
    
    private init() {}
    
    func searchPhotos(_ keyword: String, _ completion: @escaping ([ImageItem]?, Error?) -> Void) {
        guard let url = URL(string: baseUrl + "&q=\(keyword.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "+"))&per_page=200") else {
            completion(nil, ServerError.malformedUrl)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        performRequest(request) { data, error  in
            guard let data = data, let response = try? JSONDecoder().decode(ImagesResponse.self, from: data) else {
                completion(nil, error ?? ServerError.noData)
                return
            }
            completion(response.hits, nil)
        }
    }

    
    private func performRequest(_ request: URLRequest, completion: @escaping (Data?, Error?) -> Void) {
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { (data, _, error) in
            completion(data, error)
        }
        dataTask.resume()
    }
}
