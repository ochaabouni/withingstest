//
//  ServerProvider.swift
//  WithingsTest
//
//  Created by Omar Chaabouni on 02/08/2021.
//  Copyright © 2021 Omar Chaabouni. All rights reserved.
//

import Foundation

protocol ServerProvider {
    func searchPhotos(_ keyword: String, _ completion: @escaping ([ImageItem]?, Error?) -> Void)
}

enum ServerError: Error {
    case noData
    case malformedUrl
}
